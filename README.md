# Serverless Workshop

This repo is intended to have a workshop for Tumo and discuss Serverless Framework.

## Branches
There are currently 3 main Branches

`master` - initial serverless setup with a hello world function
`graphql` - graphql API with mocked data
`graphql-dynamodb` - graphql API connected to dynamodb

## Installation

First, you need to install Serverless Framework
```bash
npm install -g serverless
```
To setup Serverless run the command below, and follow the prompts. 
```bash
serverless
```
Note: you will need to provide AWS credentials

Then run
```bash
npm install
```

## Deployment
To deploy the infrastructure specified in `serverless.yml` run
```bash
serverless deploy
```

## Contribution
This project is missing some features that I would like to add in the future
- Lambdas to insert data to DynamoDb through API
- Tests